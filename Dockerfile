FROM python:latest

RUN apt update && apt install -y \
	python3-pip \
	iproute2

COPY . /code
RUN pip3 install -r /code/requirements.txt

# runtime environment
ENV inface=eth1.10
ENV outface=eth3.10

CMD python3 /code/test.py $inface $outface
