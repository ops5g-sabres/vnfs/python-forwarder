# Build:

```
sudo docker build -f ./Dockerfile -t docker.io/isilincoln/py-limiter:latest .
sudo docker push docker.io/isilincoln/py-limiter:latest
```

# Run:

```
sudo docker run -d --privileged --network host -e "inface=eth1.10" -e "outface=eth3.10" isilincoln/py-limiter:latest
```
or
```
sudo docker run -it --privileged --network host -e "inface=eth1.10" -e "outface=eth3.10" isilincoln/py-limiter:latest /bin/bash
```
