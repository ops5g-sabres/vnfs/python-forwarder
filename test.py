import os
import ipaddress
import pdb
import threading
import sys

from pypacker import psocket
from pypacker.layer12 import ethernet
from pypacker.layer3 import ip
from pypacker.layer4 import tcp, udp


if len(sys.argv) != 3:
	print("{} requires <in> <out> interfaces for forwarding", sys.argv[0])
	sys.exit(1)

input_iface_name =  sys.argv[1]
output_iface_name = sys.argv[2]

input_iface = psocket.SocketHndl(iface_name=input_iface_name, timeout=10)
output_iface = psocket.SocketHndl(iface_name=output_iface_name, timeout=10)


# newline at end of line
in_ipv4 = os.popen('ip -br -4 addr show %s' % input_iface_name).read().split(" ")[-2]
out_ipv4 = os.popen('ip -br -4 addr show %s' % output_iface_name).read().split(" ")[-2]

in_mac = os.popen('ip -br link show %s' % input_iface_name).read().split(" ")[-3]
out_mac = os.popen('ip -br link show %s' % output_iface_name).read().split(" ")[-3]

in_neigh =	os.popen('ip neigh show dev %s' % input_iface_name).read().split(" ")[-2]
out_neigh =	os.popen('ip neigh show dev %s' % output_iface_name).read().split(" ")[-2]


inD = {
	"name": input_iface_name,
	"int": input_iface,
	"ip": in_ipv4,
	"mac": in_mac,
	"neigh": in_neigh,
}

outD = {
	"name": output_iface_name,
	"int": output_iface,
	"ip": out_ipv4,
	"mac": out_mac,
	"neigh": out_neigh,
}

print(in_ipv4, out_ipv4, in_mac)
print(in_neigh, out_neigh)

# 192.168.2.0/24 via 192.168.1.32 dev eth3.10 
rtable = {}
rtable_out = os.popen('ip route').read().split('\n')
for line in rtable_out:
	if line == "" or len(line.split(' ')) < 2:
		continue
	ls = line.split(' ')
	# route = ls[0], dev ls[-1]
	route = ls[0]
	if ls[0] != "default":
		route = ipaddress.IPv4Network(ls[0])
	else:
		route = ipaddress.IPv4Network("0.0.0.0/0")
	rtable[route] = ls[-2]

#print(rtable)

def filter_pkt(this_mac):
	return pkt.eth.dst == this_mac

def forward_iface(inD, outD):
	while True:
		for raw_bytes in inD["int"]:
			eth = ethernet.Ethernet(raw_bytes)
			#print("%s Got packet: %r: %s %s" % (inD["name"], eth, eth.src_s, eth.dst_s))

			eth.src_s = outD["mac"]
			eth.dst_s = outD["neigh"]
			#if eth[tcp.TCP] is not None:
			#		eth[tcp.TCP]._calc_sum()
			#		eth[ip.IP]._update_fields()
			if eth[udp.UDP] is not None:
					eth[udp.UDP]._calc_sum()
					eth[ip.IP]._update_fields()
			outD["int"].send(eth.bin())


x = threading.Thread(target=forward_iface, args=(inD, outD))
x.start()
y = threading.Thread(target=forward_iface, args=(outD, inD))
y.start()

x.join()
y.join()

#while True:
	#ip_pkt = eth[ip.IP]
	#pdb.set_trace()

	# Send bytes
	#input_iface.send(eth.bin())
	# Receive raw bytes
	#bts = input_iface.recv()
	# Send/receive based on source/destination data in packet
	#pkts = input_iface.sr(ip_pkt)
	# Use filter to get specific packets
	#pkts = input_iface.recvp(filter_match_recv=filter_pkt)
	# stop on first packet

input_iface.close()
output_iface.close()
